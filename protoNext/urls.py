from django.urls import include, path
from .views import index

app_name = 'protoNext'

urlpatterns = [path('', index, name='index'),]