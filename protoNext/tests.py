from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

# Create your tests here.
class UnitTest(TestCase):
    def test_url_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_index_template(self):
        response= Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_url_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.browser.quit()

    def test_press_accordion_button(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('', self.browser.title)
        time.sleep(5)



