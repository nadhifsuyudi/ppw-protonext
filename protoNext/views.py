from django.shortcuts import render
from django.http import HttpResponseRedirect, request


# Create your views here.
def index(request):
    return render(request, 'home.html')
