from django.apps import AppConfig


class ProtonextConfig(AppConfig):
    name = 'protoNext'
